          
; =============================================================================
; TITLE    : DINO RUN
; AUTHOR   : Tomeu Ramis Tarrag� & Josep Borr�s S�nchez
; CREATION : 24-DESEMBRE-2018
; =============================================================================
            ORG     $1000

; --- CODE INCLUDES -----------------------------------------------------------
            
            INCLUDE "SYSCONST.X68"          ; SYSTEM CONSTANTS
            INCLUDE "SYSTEM.X68"            ; SYSTEM CODE
            INCLUDE "CONST.X68"             ; USER CONSTANTS
            INCLUDE "UTIL.X68"              ; UTILITY MACRO AND SUBROUTINES
            INCLUDE "PLAYER.X68"            ; PLAYER MANAGEMENT
            INCLUDE "AGENTLST.X68"          ; AGENT LIST MANAGEMENT
            INCLUDE "CACTUS.X68"            ; CACTUS AGENT MANAGEMENT
            INCLUDE "ASTEROID.X68"          ; ASTEROID AGENT MANAGMENT
            INCLUDE "FLAMES.X68"            ; ASTEROID FLAMES
            INCLUDE "SPAWNER.X68"           ; SPAWNER (ASTEROIDS & CACTUS)
            INCLUDE "XPLOSION.X68"          ; EXPLOSION AGENT MANAGEMENT
            INCLUDE "STATES.X68"            ; GAME STATES MANAGEMENT
            INCLUDE "LIFE.X68"              ; LIVES OF THE PLAYER
            INCLUDE "BACKGROUND.X68"        ; BACKGROUND
            INCLUDE "MUSIC.X68"             ; MUSIC AND SOUND EFECTS
            INCLUDE "FILEOUT.X68"           ; FILE OUT MANAGER

; --- INITIALIZE --------------------------------------------------------------

START       JSR     SYSINIT                 ; INITIALIZE SYSTEM CODE
            JSR     STAINIT                 ; INITIALIZE GAME STATE

; --- UPDATE ------------------------------------------------------------------

.LOOP       TRAP    #KBDTRAP                ; READ KEYBOARD
            JSR     STAUPD                  ; UPDATE DEPENDING ON THE STATE

; --- WAIT SYNCH --------------------------------------------------------------

.WINT       TST.B   (SCRINTCT)              ; WAIT FOR INTERRUPT
            BEQ     .WINT
            CLR.B   (SCRINTCT)

; --- PLOT --------------------------------------------------------------------

            JSR     STAPLOT                 ; PLOT DEPENDING ON THE STATE
            TRAP    #SCRTRAP                ; SHOW GRAPHICS
            BRA     .LOOP
            SIMHALT

; --- VARIABLES ---------------------------------------------------------------

            INCLUDE "SYSVAR.X68"
            INCLUDE "VAR.X68"
            END    START















































*~Font name~Courier New~
*~Font size~10~
*~Tab type~0~
*~Tab size~4~
