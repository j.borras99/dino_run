; =============================================================================
; GAME STATE MANAGEMENT
; =============================================================================

; -----------------------------------------------------------------------------
STAINIT
; INITALIZES THE STATE MANAGER
; INPUT    - NONE
; OUTPUT   - NONE
; MODIFIES - NONE
; -----------------------------------------------------------------------------
            MOVE.W  #STANONE,(STACUR)
            MOVE.W  #STAINTR,(STANEXT)
            RTS
; -----------------------------------------------------------------------------
STAUPD
; PERFORMS STATE UPDATE AND INIT IF NECESSARY
; INPUT    - NONE
; OUTPUT   - NONE
; MODIFIES - NONE
; -----------------------------------------------------------------------------
            MOVEM.L D0/A0,-(A7)
            CLR.L   D0
            MOVE.W  (STANEXT),D0
            CMP.W   (STACUR),D0
            BEQ     .DOUPD
            MOVE.W  D0,(STACUR)             ; IF STATE CHANGED, DO INIT
            LSL.L   #2,D0
            MOVE.L  D0,A0
            MOVE.L  .INITBL(A0),A0
            JSR     (A0)
.DOUPD      CLR.L   D0                      ; IN ANY CASE, DO UPDATE
            MOVE.W  (STACUR),D0
            LSL.L   #2,D0
            MOVE.L  D0,A0
            MOVE.L  .UPDTBL(A0),A0
            JSR     (A0)
            MOVEM.L (A7)+,D0/A0
            RTS
.INITBL     DC.L    STAINTRI,STAHELPI,STAPLAYI,STAEMPTY
.UPDTBL     DC.L    STAINTRU,STAHELPU,STAPLAYU,STAGOVERU

; -----------------------------------------------------------------------------
STAPLOT
; PERFORMS STATE PLOT
; INPUT    - NONE
; OUTPUT   - NONE
; MODIFIES - NONE
; -----------------------------------------------------------------------------
            MOVEM.L D0/A0,-(A7)
            CLR.L   D0
            MOVE.W  (STACUR),D0
            LSL.L   #2,D0
            MOVE.L  D0,A0
            MOVE.L  .PLTTBL(A0),A0
            JSR     (A0)
            MOVEM.L (A7)+,D0/A0
            RTS
.PLTTBL     DC.L    STAINTRP,STAHELPP,STAPLAYP,STAGOVERP

; -----------------------------------------------------------------------------
STAEMPTY
; EMPTY SUBROUTINE FOR CASES WITH NOTHING TO DO
; INPUT    - NONE
; OUTPUT   - NONE
; MODIFIES - NONE
; NOTE     - JUST A PLACEHOLDER. NOTHING TO DO.
; -----------------------------------------------------------------------------
            RTS

; -----------------------------------------------------------------------------
STAPLAYI
; PLAY STATE INITIALIZATION.
; INPUT    - NONE
; OUTPUT   - NONE
; MODIFIES - NONE
; NOTE     - JUST A WRAPPER FOR PLRINIT
; -----------------------------------------------------------------------------
            JSR     MUSCBKG    
            JSR     BKGRINIT
            JSR     PLRINIT
            RTS
; -----------------------------------------------------------------------------
STAPLAYU
; PLAY STATE UPDATE
; INPUT    - NONE
; OUTPUT   - NONE
; MODIFIES - NONE
; -----------------------------------------------------------------------------
            JSR     BKGRUPD
            JSR     DRWBG
            JSR     PLRUPD
            JSR     SPWUPD
            BRA     AGLUPD

; -----------------------------------------------------------------------------
STAPLAYP
; PLAY STATE PLOT
; INPUT    - NONE
; OUTPUT   - NONE
; MODIFIES - NONE
; -----------------------------------------------------------------------------
            JSR     AGLPLOT
            BRA     PLRPLOT

; -----------------------------------------------------------------------------
STAINTRI    EQU     DMMINIT
; INTRO STATE INIT
; INPUT    - NONE
; OUTPUT   - NONE
; MODIFIES - NONE
; NOTE     - JUST A WRAPPER FOR DMMINIT TO RELEASE ALL MEMORY
; -----------------------------------------------------------------------------

; -----------------------------------------------------------------------------
STAINTRU
; INTRO STATE UPDATE
; INPUT    - NONE
; OUTPUT   - NONE
; MODIFIES - NONE
; -----------------------------------------------------------------------------
            JSR     MUSCSTOP
            BTST.B  #KBDBITJM,(KBDEDGE)
            BEQ     .HLP
            MOVE.W  #STAPLAY,(STANEXT)
            BRA     .END
.HLP        BTST.B  #KBDBITHP, (KBDEDGE)
            BEQ     .END
            MOVE.W  #STAHELP, (STANEXT)
.END        RTS

; -----------------------------------------------------------------------------
STAINTRP
; INTRO STATE PLOT
; INPUT    - NONE
; OUTPUT   - NONE
; MODIFIES - NONE
; -----------------------------------------------------------------------------
            MOVEM.L A1/D0,-(A7)
            UTLLOCT 31,5
            UTLSPEN #$00000000
            UTLSFIL #$00000000
            LEA     .TITLE,A1
            MOVE.B  #14,D0
            TRAP    #15
            UTLLOCT 28,15
            UTLSPEN #$00000000
            UTLSFIL #$00000000
            LEA     .PRSSHLP,A1
            MOVE.B  #14,D0
            TRAP    #15
            
            UTLLOCT 28,17
            UTLSPEN #$00000000
            UTLSFIL #$00000000
            LEA     .PRSSSTR, A1
            MOVE.B  #14, D0
            TRAP    #15
            
            UTLLOCT 0,31
            UTLSPEN #INTAUTH
            UTLSFIL #INTAUTH
            LEA     .AUTHOR, A1
            MOVE.B  #14, D0
            TRAP    #15
            MOVEM.L (A7)+,A1/D0
            RTS

.TITLE      DC.B    'DINO RUN 68000',0
.PRSSHLP    DC.B    'Press H for help',0
.PRSSSTR    DC.B    'Press space to START',0
.AUTHOR     DC.B    'Bartomeu Ramis ~ Josep Borr�s',0
            DS.W    0
; -----------------------------------------------------------------------------
STAHELPI
; HELP STATE INITIALIZATION.
; INPUT    - NONE
; OUTPUT   - NONE
; MODIFIES - NONE
; -----------------------------------------------------------------------------
            RTS
; -----------------------------------------------------------------------------
STAHELPU
; HELP STATE UPDATE
; INPUT    - NONE
; OUTPUT   - NONE
; MODIFIES - NONE
; -----------------------------------------------------------------------------
            BTST.B  #KBDBITJM,(KBDEDGE)
            BEQ     .END
            MOVE.W  #STAINTR,(STANEXT)
.END        RTS

; -----------------------------------------------------------------------------
STAHELPP
; HELP STATE PLOT
; INPUT    - NONE
; OUTPUT   - NONE
; MODIFIES - NONE
; -----------------------------------------------------------------------------
            MOVEM.L D0-D1/A1, -(A7)
            UTLLOCT 14,10
            UTLSPEN #$00000000 
            UTLSFIL #$00000000           
            LEA     .P1, A1
            MOVE.B  #14, D0
            TRAP    #15
            
            UTLLOCT 14,12
            UTLSPEN #$00000000 
            UTLSFIL #$00000000 
            LEA     .P2, A1
            MOVE.B  #14, D0
            TRAP    #15
   
            UTLLOCT 14,14
            UTLSPEN #$00000000 
            UTLSFIL #$00000000 
            LEA     .P3, A1
            MOVE.B  #14, D0
            TRAP    #15
            
            UTLLOCT 26,24
            UTLSPEN #INTHLP
            UTLSFIL #INTHLP 
            LEA     .P4, A1
            MOVE.B  #14, D0
            TRAP    #15
            
            UTLLOCT 0, 31
            UTLSPEN #$00000000 
            UTLSFIL #$00000000 
            LEA     .P5, A1
            MOVE.B  #14, D0
            TRAP    #15
            
            MOVEM.L (A7)+, D0-D1/A1
            RTS
.P1         DC.B    '�Per moure el personatge utilitza les fletxes del teclat.',0
.P2         DC.B    '�Per saltar utilitza la barra; espai.',0
.P3         DC.B    '�Evita els obstacles que aniran apareixent.',0
.P4         DC.B    'MOLTA SORT I ENDAVANT !',0
.P5         DC.B    'Press space to go back',0
            DS.W    0
; -----------------------------------------------------------------------------
STAGOVERU
; GAME OVER STATE UPDATE
; INPUT    - NONE
; OUTPUT   - NONE
; MODIFIES - NONE
; -----------------------------------------------------------------------------
            MOVEM.L D0-D2, -(A7)
            
            JSR     FILEWRITE
            
            BTST.B  #KBDBITJM,(KBDEDGE)
            BEQ     .END
            MOVE.W  #STAINTR,(STANEXT)
            
.END        MOVEM.L (A7)+, D0-D2
            RTS
            

; -----------------------------------------------------------------------------
STAGOVERP
; GAME OVER STATE PLOT
; INPUT    - NONE
; OUTPUT   - NONE
; MODIFIES - NONE
; -----------------------------------------------------------------------------
            MOVEM.L A1/D0-D2,-(A7)
            
            UTLLOCT 35,15
            UTLSPEN #$00000000
            UTLSFIL #$00000000
            LEA     .GOVSTR,A1
            MOVE.B  #14,D0
            TRAP    #15
            
            UTLLOCT 34,17
            UTLSPEN #$00000000 
            UTLSFIL #$00000000 
            LEA     .SCR1, A1
            MOVE.B  #14, D0
            TRAP    #15
                       
            LEA     .SCR2, A1
            JSR     UTLSCR
            UTLLOCT 42,17
            UTLSPEN #$00000000 
            UTLSFIL #$00000000 
            MOVE.B  #14, D0
            TRAP    #15
            
            MOVEM.L (A7)+,A1/D0-D2
            RTS

.GOVSTR     DC.B    'GAME  OVER',0
.SCR1       DC.B    'SCORE: ',0
.SCR2       DS.B    4
            DC.B    0
            DS.W    0

















































*~Font name~Courier New~
*~Font size~10~
*~Tab type~0~
*~Tab size~4~
